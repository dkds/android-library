# android-library

Android Widgets and Support Class Library

# Class List

* [OpenFileDialog.java](./src/main/java/com/github/axet/androidlibrary/widgets/OpenFileDialog.java)

Android Directory / File Choicer Dialog with options to create / rename / delete file.

![openfiledialog.png](./imgs/openfiledialog.png)

* [PopupShareActionProvider.java](./src/main/java/com/github/axet/androidlibrary/widgets/PopupShareActionProvider.java)

![popupshareactionprovider.png](./imgs/popupshareactionprovider.png)

* [SeekBarPreference.java](./src/main/java/com/github/axet/androidlibrary/widgets/SeekBarPreference.java)

![seekbarpreference.png](./imgs/seekbarpreference.png)

Expandable List View

* [RemoveItemAnimation.java](./src/main/java/com/github/axet/androidlibrary/animations/RemoveItemAnimation.java)

![removeitemanimation.gif](./imgs/removeitemanimation.gif)

* [MarginAnimation.java](./src/main/java/com/github/axet/androidlibrary/animations/MarginAnimation.java)

![expanditem.gif](./imgs/expanditem.gif)

WebViewCustom

* [WebViewCustom.java](./src/main/java/com/github/axet/androidlibrary/widgets/WebViewCustom.java)

Support Apache HttpClient, Proxying...

* [UnreadCountDrawable.java](./src/main/java/com/github/axet/androidlibrary/widgets/UnreadCountDrawable.java)

Unread Count Drawable

![./imgs/unread.png](./imgs/unread.png)

# Install

## gradle

```gradle
    compile 'com.github.axet:android-library:1.35.5'
```

## maven

```xml
<dependency>
  <groupId>com.github.axet</groupId>
  <artifactId>android-library</artifactId>
  <version>1.35.5</version>
  <type>aar</type>
</dependency>
```
